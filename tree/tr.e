#include <tree.h>

template<typename K, typename V>
Node<K, V>::Node(K key, V value) 
{
    this->_key = key;
    this->_value = value;
    this->_left = nullptr;
    this->_right = nullptr;
}

template<typename K, typename V>
Node<K, V>::~Node()
{

}

template<typename K, typename V>
void Node<K, V>::put(K key, V value)
{
    if (this->shouldGoLeft(key)) 
        this->putLeft(key, value);       
    else this->putRight(key, value);
}

template<typename K, typename V>
inline bool Node<K, V>::shouldGoLeft(K key)
{
    return this->_key > key;
}

template<typename K, typename V>
V& Node<K, V>::find(K key) 
{
    if (this->_key == key)
        return this->_value;
    if (this->shouldGoLeft(key))
        return this->_left.get()->find(key);
    else return this->_right.get()->find(key);
}

template<typename K, typename V>
Node<K, V>& Node<K, V>::nodeforKey(K key)
{
    if(this->_key == key)
        return *this;
}

template <typename K, typename V>
inline bool Node<K, V>::nodeExists(std::unique_ptr<Node> node)
{
    return node != nullptr;
}

template<typename K, typename V>
inline bool Node<K, V>::leftNodeExists()
{
    return Node<K, V>::nodeExists(this->_left);
}

template<typename K, typename V>
inline bool Node<K, V>::rightNodeExists()
{
    return Node::nodeExists(this->_left);
}

template<typename K, typename V>
void Node<K, V>::putLeft(K key, V value) 
{
    this->putIn(this->_left, key, value);
}

template<typename K, typename V>
void Node<K, V>::putRight(K key, V value)
{
    this->putIn(this->_right, key, value);
}

template<typename K, typename V>
void Node<K, V>::putIn(std::unique_ptr<Node>& ptr, K key, V value)
{
    if (ptr == nullptr)
        ptr = std::make_unique<Node>(key, value);
    else
        ptr.get()->put(key, value);
}
