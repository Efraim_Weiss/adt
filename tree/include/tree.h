#pragma once 
#include<memory>
#include<cstdlib>
template<typename K,typename V> 
class Node {
    public:
        Node(K, V);
        ~Node();
        
        void put(K, V);
        Node<K, V>* remove(K);
       
        Node<K, V>* findNode(K);
        V* findValue(K);
    
        bool leftNodeExists();
        bool rightNodeExists();
    
        K getKey();
        V getValue();

    private:
        inline void putLeft(K, V);
        inline void putRight(K, V);
        inline void putIn(std::unique_ptr<Node>&, K, V);
        inline bool shouldGoLeft(K);
        

        static inline Node<K, V> * removeNodeIfEqual(std::unique_ptr<Node>&, K key);
        static inline bool nodeExists(std::unique_ptr<Node>&);
    private:
        K _key;
        V _value;
        std::unique_ptr<Node> _left;
        std::unique_ptr<Node> _right;
};



template<typename K, typename V>
Node<K, V>::Node(K key, V value) 
{
    this->_key = key;
    this->_value = value;
    this->_left = nullptr;
    this->_right = nullptr;
}

template<typename K, typename V>
Node<K, V>::~Node()
{

}



template<typename K, typename V>
K Node<K, V>::getKey()
{
    return this->_key;
}

template<typename K, typename V>
V Node<K, V>::getValue()
{
    return this->_value;
}

template<typename K, typename V>
void Node<K, V>::put(K key, V value)
{
    if (this->_key == key)
    {
        this->_value = value;
        return;
    }
    if (this->shouldGoLeft(key)) 
        this->putLeft(key, value);
    else this->putRight(key, value);
}

template<typename K, typename V>
inline bool Node<K, V>::shouldGoLeft(K key)
{
    return this->_key > key;
}

template<typename K, typename V>
V* Node<K, V>::findValue(K key) 
{
    Node<K, V>* node = this->findNode(key);
    return node != nullptr ? &node->_value : nullptr;
}


template<typename K, typename V>
Node<K, V>* Node<K, V>::findNode(K key)
{
    if(this->_key == key)
        return this;

    if (this->shouldGoLeft(key) && this->leftNodeExists())
        return this->_left.get()->findNode(key);
    else if (this->rightNodeExists())
        return this->_right.get()->findNode(key);
    else
        return nullptr;
}


template<typename K, typename V>
Node<K, V> * Node<K, V>::remove(K key)
{
    if (this->_key == key)
    {
        //this = nullptr;
    }
    if (this->shouldGoLeft(key) && this->leftNodeExists())
    {
        return Node<K, V>::removeNodeIfEqual(this->_left, key);
    }
    else if (this->rightNodeExists())
    {
        return Node<K, V>::removeNodeIfEqual(this->_right, key);
    }
    return this;
}


template<typename K, typename V>
inline Node<K, V> * Node<K, V>::removeNodeIfEqual(std::unique_ptr<Node>& node, K key)
{
    if(node.get()->_key == key)
    {
        node = nullptr;
        return nullptr;
    }
    return node.get()->remove(key);
}


template <typename K, typename V>
inline bool Node<K, V>::nodeExists(std::unique_ptr<Node>& node)
{
    return node != nullptr;
}


template<typename K, typename V>
inline bool Node<K, V>::leftNodeExists()
{
    return Node<K, V>::nodeExists(this->_left);
}


template<typename K, typename V>
inline bool Node<K, V>::rightNodeExists()
{
    return Node::nodeExists(this->_right);
}

template<typename K, typename V>
void Node<K, V>::putLeft(K key, V value) 
{
    this->putIn(this->_left, key, value);
}

template<typename K, typename V>
void Node<K, V>::putRight(K key, V value)
{
    this->putIn(this->_right, key, value);
}

template<typename K, typename V>
void Node<K, V>::putIn(std::unique_ptr<Node>& ptr, K key, V value)
{
    if (ptr == nullptr)
        ptr = std::make_unique<Node>(key, value);
    else
        ptr.get()->put(key, value);
}
