#include <cstdlib>
#include <iostream>


#include "tree.h"

int main()
{
    Node<int, int> * head = new Node<int, int>(1, 5);
    head->put(3, 7);
    std::cout << "found" << *head->findValue(3) << "  "<< head << std::endl;
    head->put(1, 8);
    std::cout << "found" << *head->findValue(1) << std::endl;
    //head->tree();
    //delete head;
    head = head->remove(1);
    std::cout << "found" << head << std::endl;
}


